const fs = require('fs');
const path = require('path');

let markup = '<!doctype><html><head><title>Report</title></head><body>';

fs.readdir(path.join(__dirname, '../pixels'), (err, files) => {
    if (err) {
        return;
    }
    files.forEach(file => {
        markup += `<p>${file}</p>`;
    });

    markup += '</body></html>';

    fs.writeFileSync(path.join(__dirname, '../pixels/index.html'), markup);
});
