const fs = require('fs');
const puppeteer = require('puppeteer');

const routes = require('../routes.config');
const create = require('../src/lib/snapshot').create;

(async () => {
    if (fs.existsSync(routes.snapshotsDir)) return;

    const browser = await puppeteer.launch();
    const pending = [];

    routes.paths.forEach(route => {
        pending.push(new Promise(resolve => {
            browser.createIncognitoBrowserContext().then(context => {
                context.newPage().then(page => {
                    create(page, route, true).then(() => {
                        context.close().then(resolve);
                    });
                });
            });
        }));
    });

    await Promise.all(pending);

    await browser.close();
})();
