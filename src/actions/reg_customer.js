module.exports = async (page, user) => {
    await page.goto('https://a24.biz/reg');

    await page.type('input[name=email]', user.email);
    await page.type('input[name=nickname]', user.login);
    await page.type('input[name=password]', user.password);
    await page.type('input[name=password_confirm]', user.password);

    const submit = await page.$('form[data-js="reg-form"] button[type=submit]');

    await submit.click();

    await page.waitForNavigation({waitUntil: 'domcontentloaded'});

    return page;
};
