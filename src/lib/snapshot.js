const fs = require('fs');
const path = require('path');

const PNG = require('pngjs').PNG;
const pixelMatch = require('pixelmatch');

const regCustomer = require('../actions/reg_customer');
const routes = require('../../routes.config');

const User = require('./user');

function prettyName(route) {
    return route.replace('/', '_');
}

async function create(page, route, source = false, newUser = true) {
    const snapshotDir = path.resolve(__dirname, `../../${routes.snapshotsDir}/${prettyName(route)}`);
    const snapshotPath = path.resolve(__dirname, `${snapshotDir}/${source ? 'source' : 'actual'}.png`);

    if (newUser) {
        await regCustomer(page, new User());
    }

    await page.goto(`https://a24.biz/${route}`, {waitUntil: 'networkidle0'});

    const viewport = await page.$eval('body', body => ({
        width: body.scrollWidth,
        height: body.scrollHeight
    }));
    await page.setViewport(viewport);

    if (!fs.existsSync(routes.snapshotsDir)) {
        fs.mkdirSync(routes.snapshotsDir);
    }
    if (!fs.existsSync(snapshotDir)) {
        fs.mkdirSync(snapshotDir);
    }

    await page.screenshot({path: snapshotPath});

    return snapshotPath;
}

function compare(route, saveDiff = true) {
    return new Promise(resolve => {
        let filesRead = 0;
        const totalFiles = 2;
        const prettyRoute = prettyName(route);
        const actualPath = path.resolve(__dirname, `../../${routes.snapshotsDir}/${prettyRoute}/actual.png`);
        const sourcePath = path.resolve(__dirname, `../../${routes.snapshotsDir}/${prettyRoute}/source.png`);
        const diffPath = path.resolve(__dirname, `../../${routes.snapshotsDir}/${prettyRoute}/diff.png`);

        const actual = fs.createReadStream(actualPath).
            pipe(new PNG()).
            on('parsed', done);
        const source = fs.createReadStream(sourcePath).
            pipe(new PNG()).
            on('parsed', done);

        function done() {
            if (++filesRead < totalFiles) {
                return null;
            }

            const diff = new PNG({
                width: source.width,
                height: source.height
            });
            const diffPixelsCount = pixelMatch(
                actual.data,
                source.data,
                diff.data,
                source.width,
                source.height,
                {threshold: 0.1}
            );

            if (saveDiff) {
                diff.pack().pipe(fs.createWriteStream(diffPath));
            }

            resolve({
                pixelsCount: diffPixelsCount,
                path: diffPath
            });
            return null;
        }
    });
}

module.exports = {
    create,
    compare
};
