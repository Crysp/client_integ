const faker = require('faker');

class User {
    constructor() {
        this.email = faker.internet.email();
        this.login = (faker.name.firstName() + faker.name.lastName()).
            replace('\'', '');
        this.password = faker.internet.password();
    }

    toString() {
        return JSON.stringify({
            email: this.email,
            login: this.login,
            password: this.password
        });
    }
}

module.exports = User;
