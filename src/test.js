const snapshot = require('./lib/snapshot');

let context;
const testTimeout = 60000;

async function compareSnapshot(route) {
    const page = await context.newPage();
    await snapshot.create(page, route);
    const diff = await snapshot.compare(route);
    return diff.pixelsCount;
}

describe('Customer', async () => {
    jest.setTimeout(testTimeout);

    beforeEach(async () => {
        if (context) {
            await context.close();
        }
        context = await browser.createIncognitoBrowserContext();
    });

    describe('Dashboard', () => {
        it('Welcome', async () => {
            const expectedPixels = 5000;
            const pixelsCount = await compareSnapshot('home');
            expect(pixelsCount).toBeLessThan(expectedPixels);
        }, testTimeout);
    });

    describe('My orders', () => {
        it('Welcome', async () => {
            const expectedPixels = 6000;
            const pixelsCount = await compareSnapshot('home/myorders');
            expect(pixelsCount).toBeLessThan(expectedPixels);
        });
    });

    describe('Top authors', () => {
        it('Index', async () => {
            const expectedPixels = 7000;
            const pixelsCount = await compareSnapshot('authors');
            expect(pixelsCount).toBeLessThan(expectedPixels);
        });
    });

    describe('Order', () => {
        it('Create', async () => {
            const expectedPixels = 5000;
            const pixelsCount = await compareSnapshot('order/createorder');
            expect(pixelsCount).toBeLessThan(expectedPixels);
        });
    });
});
